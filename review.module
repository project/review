<?php

function review_help($section) {
  $output = '';

  switch ($section) {
    case 'admin/help#review':
      return t('Reviews allow users to write an article, about something and add a rating to this article. Typically this rating is set e.g. 8 from 10.');

    case 'admin/modules#description':
      return t('Allow nodes to have ratings, includes a basic review node.');

    case 'node/add#review':
      return t('Reviews are articles with a numeric rating.');
  }
}

function review_menu($may_cache) {
  $items = array();

  if ($may_cache) {
    $items[] = array(
      'path' => 'node/add/review',
      'title' => t('review'),
      'access' => user_access('create reviews'),
    );
    $items[] = array(
      'path' => 'review',
      'title' => t('reviews'),
      'type' => MENU_SUGGESTED_ITEM,
      'access' => user_access('access content'),
      'callback' => 'review_page',
    );
    $items[] = array(
      'path' => 'admin/settings/review',
      'title' => t('Review'),
      'description' => t('Review rank settings.'),
      'callback' => 'drupal_get_form',
      'callback arguments' => 'review_admin_settings',
      'access' => user_access('administer site configuration'),
      'type' => MENU_NORMAL_ITEM,
    );
  }

  return $items;
}

function review_perm() {
  return array('create reviews', 'edit own reviews');
}

function review_admin_settings() {
  $form['review_text'] = array(
    '#type' => 'textarea',
    '#title' => t('Review list header'),
    '#default_value' => variable_get('review_text', ''),
    '#cols' => 55,
    '#rows' => 5,
    '#description' => 'This text will show up above the browse pages of the reviews'
  );
  $form['review_rating_max'] = array(
    '#type' => 'select',
    '#title' => t('Rating'),
    '#default_value' => variable_get('review_rating_max', 10),
    '#options' => array(1 => '0 to 1', 2 => '0 to 2', 3 => '0 to 3',  4 => '0 to 4', 5 => '0 to 5', 6 => '0 to 6', 7 => '0 to 7',  8 => '0 to 8', 9 => '0 to 9', 10 => '0 to 10'),
    '#description' => t("The scale on wich reviewed items are rated. Note that if this is changed when there are already reviews in the database, the existing ratings will become inaccurate. For example, if there is a review with the rating of 7 out of 10, and you later change the settings into '0 to 5', visitors will see: '7 out of 5'.")
  );
  $form['review_rating_format'] = array(
    '#type' => 'select',
    '#title' => t('Show review rate'),
    '#default_value' => variable_get('review_rating_format', 'text'),
    '#options' => array('text' => 'Text', 'start' => 'Stars'),
    '#description' => t("The way the rating is formatted.")
  );
  return system_settings_form($form);
}

function review_node_info() {
  return array(
    'review' => array(
      'name' => t('Review'),
      'module' => 'review',
      'description' => t('Reviews are articles with a numeric rating.')
    )
  );
}

function review_access($op, $node) {
  global $user;

  if ($op == 'create') {
    return user_access('create reviews');
  }

  if ($op == 'update' || $op == 'delete') {
    if (user_access('edit own reviews') && ($user->uid == $node->uid)) {
      return TRUE;
    }
  }
}

function review_form(&$node) {
  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#size' => 60,
    '#maxlength' => 128,
    '#default_value' => $node->title,
    '#required' => TRUE,
  );
  $form['body'] = array(
    '#type' => 'textarea',
    '#title' => t('Body'),
    '#rows' => 15,
    '#default_value' => $node->body,
    '#required' => TRUE,
  );
  $form['format'] = filter_form($node->format);
  return $form;
}

/**
 * Implementation of hook_form_alter().
 */
function review_form_alter($form_id, &$form) {

  // We're only modifying node forms, if the #node field isn't set we don't need
  // to bother.
  if ((!isset($form['#node']))&&(!isset($form['#node_type']))) {
    return;
  }

  // Get the node type. This might be a node form or it might be the content
  // types form.
  $node = $form['#node'];
  if ($node) $type = $node->type;
  else {
    $ntype = $form['#node_type'];
    $type = $ntype->type;
  }

  $enabled = variable_get('review_'. $type . '_page', 0);

  switch ($form_id) {
    case 'node_type_form':
      // we're a special case, always on.
      if ($type == 'review') {
        return;
      }
      else {
        $form['workflow']['review_'. $type] = array(
          '#type' => 'radios',
          '#title' => t('Rateable'),
          '#default_value' => $enabled,
          '#options' => array(0 => t('Disabled'), 1 => t('Enabled')),
          '#description' => t('Should this node have a rating attached to it?'),
        );
      }
      break;

    case $type .'_node_form':
      // If the rating is enabled for this node type (or this is a review type) insert our controls
      if ($enabled || $type == 'review') {
        $node = $form['#node'];
        // can't use null as an index so use 'unrated' instead. we'll translate
        // it back to null when the form is validated.
        $rating = (isset($node->review_rating)) ? $node->review_rating : 'unrated';
        $options = array('unrated' => t('Unrated')) + range(0, variable_get('review_rating_max', 10));
        $form['review_rating'] = array(
          '#type' => 'select',
          '#title' => t('Rating'),
          '#default_value' => $rating,
          '#options' => $options,
          '#description' => t('The Rating of the reviewed item.'),
          '#weight' => 0,
        );
      }
      break;
  }
}

/**
 * Implementation of hook_nodeapi().
 */
function review_nodeapi(&$node, $op, $teaser, $page) {
  // special case for the basic review
  $enabled = ($node->type == 'review') ?
    TRUE : variable_get('review_'. $node->type . '_page', 0);

  // Check that the administrator has enabled ratings for this node type.
  if ($enabled) {
    switch ($op) {
      case 'submit':
      case 'validate':
        // make sure it's an integer or null
        if (is_numeric($node->review_rating)) {
          $node->review_rating = (integer) $node->review_rating;
        } else {
          $node->review_rating = null;
        }
        break;

      case 'load':
        if ($obj = db_fetch_object(db_query('SELECT review_rate FROM {review} WHERE nid = %d', $node->nid))) {
          $rating = $obj->review_rate;
        }
        return array('review_rating' => $rating);
        break;

      case 'delete':
        db_query('DELETE FROM {review} WHERE nid = %d', $node->nid);
        break;

      case 'update':
        db_query('DELETE FROM {review} WHERE nid = %d', $node->nid);
        // no break, fall through to the insert.
      case 'insert':
        if (isset($node->review_rating)) {
          db_query('INSERT INTO {review} (nid, review_rate) VALUES (%d, %d)', $node->nid, $node->review_rating);
        }
        break;

      case 'view':
        if (isset($node->review_rating) && $node->review_rating != 'unrated') {
	  $node->content['rating'] = array(
	    '#value' => theme(
	      'review_rating',
	      $node->review_rating,
	      variable_get('review_rating_max', 10)
	    ),
	    '#weight' => 10,
	  );
        }
        break;
    }
  }
}

function review_block($op = 'list', $delta = 0, $edit = array()) {
  switch ($op) {
    case 'list':
      $blocks[0]['info'] = t('Recent reviews');
      return $blocks;

    case 'configure':
      $form = array();
      if ($delta == 0) {
        $form['review_n_latest'] = array(
          '#type' => 'select',
          '#title' => t('Number of reviews'),
          '#default_value' => variable_get('review_n_latest', 10),
          '#options' => array(1 => '1', 2 => '2', 3 => '3',  4 => '4', 5 => '5', 6 => '6', 7 => '7',  8 => '8', 9 => '9', 10 => '10', 15 => '15', 20 => '20', 25 => '25', 30 => '30'),
          '#description' => t('The block will show this many of the most recent reviews.')
        );
      }
      return $form;

    case 'save':
      if ($delta == 0) {
        // Have Drupal save the string to the database.
        variable_set('review_n_latest', $edit['review_n_latest']);
      }
      break;

    case 'view':
      if ($delta == 0) {
        $items = array();
        $result = db_query('SELECT n.nid FROM {review} rv INNER JOIN {node} n ON rv.nid = n.nid WHERE n.status = 1 ORDER BY n.changed DESC LIMIT %d', variable_get('review_n_latest', 10));
        while ($node = db_fetch_object($result)) {
          $node = node_load($node->nid);
          $items[] = l($node->title, 'node/'. $node->nid);
        }
        $block['subject'] = t('Recent reviews');
        $block['content'] = theme('item_list', $items);
        return $block;
      }
      break;
  }
}

function review_page() {
  $output = '<div>'. check_plain(variable_get('review_text', '')) .'</div>';

  $result = pager_query('SELECT n.nid FROM {review} rv INNER JOIN {node} n ON rv.nid = n.nid WHERE n.status = 1 ORDER BY n.changed DESC');
  if (db_num_rows($result)) {
    while ($node = db_fetch_object($result)) {
      $row = array();
      $node = node_load($node->nid);
      $title = l($node->title, 'node/'. $node->nid) .' '. theme('mark', node_mark($node->nid, $node->changed));

      $categories = array();
      foreach(taxonomy_node_get_terms($node->nid) as $term) {
        //some if module existsts etc needs to be done here
        $categories[] = l($term->name, 'taxonomy/term/'. $term->tid, array('title'=>t("Show all reviews about "). $term->name));
      }
      $category= implode('<br />',$categories);

      if ($node->teaser) {
        $teaser = $node->teaser;
      }
      else {
        $teaser = substr($node->body, 0, 50);
      }

      $rate = theme('review_rating', $node->review_rating, variable_get('review_rating_max', 10));
      $rows[] = array(
        array('data' => $category, 'class' => 'Category', 'rowspan' => 2, 'valign' => 'top', 'align' => 'left'),
        array('data' => $title, 'class' => 'Title'),
        array('data' => $rate, 'class' => 'Rate', 'align'=>'left'));
      $rows[] = array(
        array('data'=>$teaser, 'class'=>'Teaser','colspan'=>2));
    }

    $header = array(t('Category'), t('Title'),  t('Rating'));
    $output .= "<div id=\"review\">";
    $output .= theme('table', $header, $rows);
    $output .= '</div>';

    $output .= theme('pager', NULL, variable_get('default_nodes_main', 10));
  } else {
    $output .= "No reviews were found...";
  }
  return $output;
}

function theme_review_rating($rating, $possible) {
  if (variable_get('review_rating_format', 'text') == 'text') {
    $rating_format = 'review_text_rating';
  } else {
    $rating_format = 'review_graphic_rating';
  }
  $output = '<div class="rate">';
  $output .= theme($rating_format, $rating, $possible);
  $output .= '</div>';
  return $output;
}

function theme_review_text_rating($rating, $possible) {
  return t('Rating: %rating of %possible', array('%rating' => $rating, '%possible' => $possible));
}

function theme_review_graphic_rating($rating, $possible) {
  global $base_url;

  for($i = 0; $i < $rating; $i++) {
    $output .= '<img class="rateimage" alt="*" title="star"';
    $output .= ' src="'. $base_url .'/'. drupal_get_path('module', 'review') . '/images/star_on.gif" />';
  }
  for($i = $rating; $i < $possible; $i++){
    $output .= '<img class="rateimage" alt=" " title="no star"';
    $output .= ' src="'. $base_url .'/'. drupal_get_path('module', 'review') . '/images/star_off.gif" />';
  }
  return $output;
}


/*
 * Views support.
 */
function review_views_tables() {
  $tables['review'] = array(
    'name' => 'review',
    'provider' => 'external', // won't show up in external list.
    'join' => array(
      'left' => array(
        'table' => 'node',
        'field' => 'nid'
      ),
      'right' => array(
        'field' => 'nid'
      ),
    ),
    'fields' => array(
      'review_rate' => array(
        'name' => t('Review: Rating'),
        'handler' => array(
          'review_handler_field_text'  => t('Text'),
          'review_handler_field_stars' => t('Graphic')
        ),
        'sortable' => true,
        'help' => t('Display review ratings.'),
      ),
    ),
    'sorts' => array(
      'review_rate' => array(
        'name' => t('Review: Rating'),
        'help' => t('This allows you to sort by review ratings.'),
      )
    ),
  );
  return $tables;
}


/*
 * Format a review rating with text.
 */
function review_handler_field_text($fieldinfo, $fielddata, $value, $data) {
  return theme(
    'review_text_rating', $value, variable_get('review_rating_max', 10)
  );
}


/*
 * Format a review rating with stars.
 */
function review_handler_field_stars($fieldinfo, $fielddata, $value, $data) {
  return theme(
    'review_graphic_rating', $value, variable_get('review_rating_max', 10)
  );
}
