Reviews allow users to write an article, about something and add a rating to
this article. Typically this rating is set e.g. 8 from 10.

In the settings page there are nine settings that can be set. They are self
explaining. However, when you choose to use images to show up as rating, keep
in mind that you need to specify those images.

Please note that this module is due to big changes. If the multiple nodes types
concept (dynamic generated node types) is introduced this module might totally
dissapear.
